#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include<unordered_set>
#include <arpa/inet.h>
#include <map>
#include<cstring>
#include <assert.h>
#include <chrono>

/**
    firewall.cpp
    Purpose: Check if a packet is accepted
    @author Sai Eshwar Prasad Muppalla
    @version 1.0
*/

//Class to get the comma separated values
class CSVRow
{
public:
    std::string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    std::size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(std::istream& str)
    {
        std::string line;
        std::getline(str, line);

        std::stringstream lineStream(line);
        std::string cell;

        m_data.clear();
        while(std::getline(lineStream, cell, ','))
        {
            m_data.push_back(cell);
        }
        // This checks for a trailing comma with no data after it.
        if (!lineStream && cell.empty())
        {
            // If there was a trailing comma then add an empty element.
            m_data.push_back("");
        }
    }
private:
    std::vector<std::string> m_data;
};


//Implementation of Interval tree
class IntervalTree
{
public:
	// Structure to represent an interval
    struct Interval
    {
        long low, high;
    };

    // Structure to represent a node in Interval Search Tree
    struct ITNode
    {
        Interval *i;  // 'i' could also be a normal variable
        int max;
        ITNode *left, *right;

    };
    ITNode *root = NULL;
    Interval * i;
    ITNode* newNode(Interval i);
    ITNode* insert(ITNode *root, Interval i);
    Interval* overlapSearch(ITNode *root, Interval i);
    bool doOVerlap(Interval i1, Interval i2) ;
    void inorder(ITNode *root);
};

/**
    Displays the in-order traversal of a tree
    @param root Root of the tree.
    @return No return.
*/
void IntervalTree::inorder(ITNode *root)
{
    if (root == NULL) return;

    inorder(root->left);

    std::cout << "[" << root->i->low << ", " << root->i->high << "]"<< " max = " << root->max << std::endl;

    inorder(root->right);
}

// A utility function to create a new Interval Search Tree Node
IntervalTree::ITNode* IntervalTree::newNode(Interval i)
{
    ITNode *temp = new ITNode;
    temp->i = new Interval(i);
    temp->max = i.high;
    temp->left = temp->right = NULL;
};

IntervalTree::ITNode* IntervalTree::insert(ITNode *root, Interval i)
{
    // Base case: Tree is empty, new node becomes root
    if (root == NULL)
        return newNode(i);

    // Get low value of interval at root
    int l = root->i->low;

    // If root's low value is smaller, then new interval goes to
    // left subtree
    if (i.low < l)
        root->left = insert(root->left, i);

    // Else, new node goes to right subtree.
    else
        root->right = insert(root->right, i);

    // Update the max value of this ancestor if needed
    if (root->max < i.high)
        root->max = i.high;

    return root;
}

// A utility function to check if given two intervals overlap
bool IntervalTree::doOVerlap(Interval i1, Interval i2)
{
    //if (i1.low <= i2.high && i2.low <= i1.high)
    if ((i1.low <= i2.low && i2.low<=i1.high) && (i1.low <= i2.high && i2.high<=i1.high))
        return true;
    return false;
}

// The main function that searches a given interval i in a given
// Interval Tree.
IntervalTree::Interval* IntervalTree::overlapSearch(ITNode *root, Interval i)
{
    // Base Case, tree is empty
    if (root == NULL) return NULL;

    // If given interval overlaps with root
    if (doOVerlap(*(root->i), i))
        return root->i;

    // If left child of root is present and max of left child is
    // greater than or equal to given interval, then i may
    // overlap with an interval is left subtree
    if (root->left != NULL && root->left->max >= i.low)
        return overlapSearch(root->left, i);

    // Else interval can only overlap with right subtree
    return overlapSearch(root->right, i);
}

std::istream& operator>>(std::istream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}


//Implementation of Firewall 
class FireWall
{
public:
    FireWall(const std::string& file_name); //Construct to read csv file
    void storeData(std::ifstream& str); //Store data into Data structure
    std::vector<std::string> getPortRange(const std::string& port);
    std::vector<std::string> splitByDelim(const std::string& str, char delim);
    std::vector<std::string> getIpRange(const std::string& port);
    void getAllPorts(std::vector<std::string> port_range, std::vector<std::string> &ports);
    void getAllIp(std::vector<std::string> Ip_range, std::vector<long> &ips);
    long convertIpToLong(std::string  ip);
    std::string convertLongToIp(long address);
    void populateValues(std::vector<long> &Ips, long &l, long &h);
	//Insert row into Data structure
    void insert(int port_value, long l, long h,std::string d,std::string p); 
    bool accept_packet(std::string d, std::string p, int port, std::string ip);

private:
    bool isIpRange = false;
    std::vector<std::string> port_range; //Vector to store port range
    std::vector<std::string> ip_range;//Vector to store Ip range
    typedef std::map<std::string,  IntervalTree> ipMap; //map(key = protocol, value = IpRangeTree)
    typedef std::map<std::string, ipMap> directionMap;//map(key = direction, value = ipMap)
    typedef std::map<int, directionMap> portMap; //map(key = port, value = directionMap)
    portMap m;
	//Iterators used for above maps
    std::map<std::string, IntervalTree>::iterator ipMapIterator; 
    std::map<std::string, ipMap>::iterator directionMapIterator;
    std::map<int, directionMap>::iterator portMapIterator;
};

/**
    Helper function to split the string by a delimiter.
    @param str String to be split.
	@param delimiter Character as a delimiter 
    @return vector of strings after split.
*/
std::vector<std::string> FireWall::splitByDelim(const std::string& str, char delimiter)
{
    auto i = 0;
    std::vector<std::string> split_list;
    auto pos = str.find(delimiter); // get position of delimiter
	//Iterate over the string by delimiter position
    while(pos != std::string::npos)
    {
        split_list.push_back(str.substr(i, pos-i));
        i = ++pos;
        pos = str.find(delimiter, pos);
    }
    split_list.push_back(str.substr(i, str.length()));
    return split_list;

}

//Convert IP to numerical value
long FireWall::convertIpToLong(std::string ip)
{
    long long_address = inet_addr (ip.c_str()) ;
    return long_address;
}

//Convert the Numerical value back to IP
std::string FireWall::convertLongToIp(long address)
{
    struct in_addr addr;
    addr.s_addr = address;
    char *dot_ip = inet_ntoa(addr);
    std::string ip = dot_ip;
    return ip;
}

//Function to get the vector of given port range.
//Returns vector containing only one value if it has no delimiter.
std::vector<std::string> FireWall::getPortRange(const std::string& port)
{
    std::vector<std::string>port_range;
	//Condition to check if it is range or not
    if (port.find('-') != std::string::npos)
    {
        port_range = splitByDelim(port, '-'); //return vector of port range
    } else
    {
        port_range.push_back(port);
    }
    return port_range;
}

//Function to get the vector of given IP range.
//Returns vector containing only one value if it has no delimiter.
std::vector<std::string> FireWall::getIpRange(const std::string& ip)
{
    std::vector<std::string>ip_range;
    std::vector<std::string>ips = {};
	//Condition to check if it has delimiter
    if (ip.find('-') != std::string::npos)
    {
        ip_range = splitByDelim(ip, '-'); //return vector of IP range
        isIpRange = true; //Boolean value used to check if it is range
    } else
    {
        ip_range.push_back(ip);
        isIpRange = false;
    }
    return ip_range;
}

//Helper function to populate "Ips" by converting to long values.
void FireWall::getAllIp(std::vector<std::string> Ip_range, std::vector<long> &Ips)
{
    auto start = *Ip_range.begin();
    long addr1 = this->convertIpToLong(start);
    Ips.push_back(addr1);
	//Add two values to a vector if given string is a range
    if(Ip_range.size() == 2)
    {
        auto end  = Ip_range.back();
        long addr2 = this->convertIpToLong(end);
        Ips.push_back(addr2);
    }
}

//Function to populate values into l and h used in tree interval.
void FireWall::populateValues(std::vector<long> &ips, long &l, long &h)
{
    if(isIpRange)
    {
        l = ips[0];
        h = ips[1];
    }
    else
    {
        l = ips[0];
        h = ips[0];
    }
}

/**
    Function to insert the record into data structure.
    @param port_value Given port value.
	@param l,h converted long values of ip range.
	@param direction String direction.
	@param protocol String protocol
    @return No return.
*/
void FireWall::insert(int port_value, long l, long h, std::string direction,std::string protocol)
{	
	//m is the hashmap of ports
	//portMapIterator is iterator for portMap
    portMapIterator = m.find(port_value); //check if port exists in hash table
    //if port does not exist
    if (portMapIterator == m.end())
    {
        IntervalTree it; //Create new tree object in stack
        it.root = it.insert(it.root, {l,h}); // Insert ip range in tree and return root
		
		//Create port as key and new direction hash table as value
        m.insert(make_pair(port_value, directionMap())); 
		
		//Create direction as key and new ip hash table as value
        m[port_value].insert(make_pair(direction, ipMap()));
		
		//Insert the root object into hastable with protocol as key.
        m[port_value][direction].insert(make_pair(protocol, it));
    } else
    {
        //if port exists in map
		//find if direction exists in nested direction HashMap
        directionMapIterator = portMapIterator->second.find(direction);
		
        //port with current direction is not there in map
        if(directionMapIterator == m[port_value].end())
        {
			//Create new tree object and add all 3 variables to nested hashmap 
            IntervalTree it;
            it.root = it.insert(it.root, {l,h});
            m[port_value].insert(make_pair(direction, ipMap()));
            m[port_value][direction].insert(make_pair(protocol, it));
        } else
        {
            //port with current direction exists in map
			//find if protocol exists in nested map
            ipMapIterator = directionMapIterator->second.find(protocol);
			
            //port with current direction and protocol is not there
            if(ipMapIterator == m[port_value][direction].end())
            {
				//Create new tree object and add rest 2 variables to nested hashmap 
                IntervalTree it;
                it.root = it.insert(it.root, {l,h});
                m[port_value][direction].insert(make_pair(protocol, it));
            } else
            {
				//If port, direction,protocol exits
                IntervalTree it = m[port_value][direction][protocol];//get root node of tree
				
				//Search if the given ip range(l,h) is in Interval tree.
                IntervalTree::Interval *res = it.overlapSearch(it.root, {l,h});

                if (res == NULL) {
                    //Not overlapping.
					//Insert into tree and return root
                    it.root = it.insert(it.root, {l,h});
					//Add root of tree to nested hashmap
					//std::cout<<port_value<<std::endl;
                    m[port_value][direction][protocol] = it;
                }
            }
        }
    }
}

/**
    Function to store each record from csv file.
    @param file Input file
    @return No return.
*/
void FireWall::storeData(std::ifstream& file)
{
    CSVRow row; //used to get the values of csv
    long l, h;
	//Iterate over each row
    while(file >> row)
    {
        port_range = getPortRange(row[2]); //Get the vector of port range split
        ip_range = getIpRange(row[3]);//Get the vector of ip range split
        std::vector<long>ips = {};
        std::string direction = row[0];
        std::string protocol = row[1];
        getAllIp(ip_range, ips); //get ips
        populateValues(ips, l, h); // populate ips into l&h
		//if port is not a range
        if(port_range.size() == 1)
        {
            int port_value = std::stoi(port_range[0]);
            insert(port_value, l, h, direction, protocol); //Insert record into data structure
        } else
        {
            int port_start = std::stoi(port_range[0]);
            int port_end = std::stoi(port_range[1]);
			//Iterate over each port in the port range
            for(int i = port_start; i<=port_end; ++i)
            {
                insert(i, l, h, direction, protocol);//Insert record into data structure
            }
        }
    }
}

//Function to check if packet is accepted.
bool FireWall::accept_packet(std::string direction, std::string protocol, int port, std::string ip)
{
    std::map<std::string, IntervalTree>::iterator ipMapIterator; //define maps and their data type
    std::map<std::string, ipMap>::iterator directionMapIterator;
    std::map<int, directionMap>::iterator portMapIterator;
    portMapIterator = m.find(port);
    long long_ip;
    //Port is there
    if (!(portMapIterator == m.end()))
    {
        directionMapIterator = portMapIterator->second.find(direction);
        //port with current direction is there in map
        if(!(directionMapIterator == m[port].end()))
        {
            ipMapIterator = directionMapIterator->second.find(protocol);
            //port with current direction and protocol is not there
            if(!(ipMapIterator == m[port][direction].end()))
            {
                auto start = ip;
                long_ip = convertIpToLong(ip);
                IntervalTree it = m[port][direction][protocol];
                IntervalTree::Interval *res = it.overlapSearch(it.root, {long_ip,long_ip});
				//If ip exists
                if (res != NULL) {
					//std::cout<<direction<<" "<<protocol<<" "<<port<<" "<<ip<<std::endl;
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
    return false;
}

//Constructor function to read and store csv file
FireWall::FireWall(const std::string& file_name)
{
    std::ifstream file(file_name);
    storeData(file);
}

int main(int argc, char* argv[])
{
	std::string file = argv[1];
    FireWall firewall(file);
	
	assert(firewall.accept_packet("outbound", "udp", 2000, "52.12.48.92") == true); 
	assert(firewall.accept_packet("outbound", "udp", 65535, "52.12.48.92") == true); 
	assert(firewall.accept_packet("outbound", "udp", 65536, "52.12.48.92") == false);
	assert(firewall.accept_packet("outbound", "udp", 0, "52.12.48.92") == false);
	assert(firewall.accept_packet("outbound", "udp", 1, "1.2.3.3") == true);
	assert(firewall.accept_packet("outbound", "udp", 1, "255.255.255.0") == true);
	assert(firewall.accept_packet("outbound", "udp", 1, "0.0.0.0") == true);
	//Edge cases
	assert(firewall.accept_packet("outbound", "tcp", 1, "0.0.0.0") == true);
	assert(firewall.accept_packet("outbound", "tcp", 0, "0.1.2.255") == false);
	assert(firewall.accept_packet("outbound", "tcp", 65536, "255.255.255.255") == false);
	
	//border cases
	assert(firewall.accept_packet("inbound", "tcp", 1, "0.0.0.0") == true);
	assert(firewall.accept_packet("inbound", "tcp", 0, "0.1.2.255") == false);
	assert(firewall.accept_packet("inbound", "tcp", 65536, "255.255.255.255") == false);	
	
	//Error scenarios
	assert(firewall.accept_packet("dcdc", "tcp", 65536, "255.255.255.255") == false);
	
	std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
	assert(firewall.accept_packet("inbound", "ddd", 65535, "255.255.255.255") == false);
	std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	std::cerr<<"Time taken validate packet "<<elapsed_seconds.count()<<std::endl;	

}
