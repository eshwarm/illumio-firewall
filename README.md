##Contents of repo

1. firewall.cpp : Implementation constructor and accept_packet
2. test.csv : File to read input
3. design.PNG : design of data structure to store each csv row.


##Command Line Arguments

  Compilation: g++ -std=c++11 firewall.cpp  -o fc 

  Execution: ./fc test.csv
  
  Output: prints time taken to return a bool value for accept_packet
  
  Test: testing is done inside main() using assert function

##how you tested your solution

 1.The input data in test.csv has all possible combinations ranging from 1-65535 for port 
   and 0.0.0.0-255.255.255.255 for ip for a given udp,tcp and inbound,outbound.

 2.Initially while writing each functions I have done Unit Testing and checked whether the given function 
   is returning as expected. This testing is done manually. For Example:
     1. checking whether the ipToLong conversion is returning as expected.
     2. Search ip range in an interval tree.

 3.Acceptance testing is done using C++ assert function to check whether the program returns with valid 
   scenarios.
   
 4.Edge cases are tested by taking the edge values 1,65535 in port values and 0.0.0.0, 255.255.255.255.
   with different combinations of direction and protocol. It is assumed to take only integer and string values.
   Hence testing is not done using floating point or string values.
   
 5.Error scenarios are tested by passing strings other than tcp,udp,inbound,outbound.



##Any interesting coding, design, or algorithmic choices you'd like to point out

**Design:**
  Nested Hashmap is used to design the data structure of input csv file.
  
  Please see the "design.png" file.
  
  As shown in the design diagram I have used nested hashtables starting with  Hash table for ports.
  
  Here port is the key and value is another Hashtable(direction).
  
  In direction hashtable , direction is the key and value is a hashtable(protocol).
  
  In protocol hashtable, protocol is the key and interval tree is the value.
    
  **Time Complexity**
    It takes O(1) time to insert and access the values into all HashTables.
	O(log n) times to insert an interval and to search an interval in an interval tree.
	
	Total time complexity to insert a csv row into data structure is O(1) (2*O(log n)) ~~ O(logn)
	Total time complexity to check if packet is accepted  ~~ O(logn)
	
  

##Any refinements or optimizations that you would have implemented if you had more time
  The time taken to insert all rows of csv into data structure takes O(n logn). It can be reduce by parallelising 
  using p_threads.
  
  
##References
 Used stack overflow to read input csv file and geeksforgeeks for interval tree implementation
